export interface UserInterface {
  username: string;
  token: string;
  id: number;
}
