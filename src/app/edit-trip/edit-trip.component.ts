import {Component, Inject, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../_services/api.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {TripInterface} from '../_models/trip.interface';

@Component({
  selector: 'app-edit-trip',
  templateUrl: './edit-trip.component.html',
  styleUrls: ['./edit-trip.component.css']
})
export class EditTripComponent implements OnInit {
  editTripForm: FormGroup;
  trip: TripInterface;
  loading: boolean;

  constructor(formBuilder: FormBuilder,
              private apiService: ApiService,
              private dialogRef: MatDialogRef<EditTripComponent>,
              @Inject(MAT_DIALOG_DATA) data) {
    this.trip = data.trip;
    this.editTripForm = formBuilder.group({
      name: [this.trip.name, Validators.required],
      description: [this.trip.description]
    });
  }

  ngOnInit(): void {
  }

  saveTrip() {
    if (this.editTripForm.invalid) {
      return;
    }
    this.loading = true;
    this.apiService.editTrip(
      this.trip.id,
      this.editTripForm.value.name,
      this.editTripForm.value.description).subscribe(() => {
        this.trip.name = this.editTripForm.value.name;
        this.trip.description = this.editTripForm.value.description
        this.dialogRef.close(true);
    });
  }
}
