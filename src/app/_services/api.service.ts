import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {apiUrl} from '../config';
import {ImageInterface, TripInterface, TripInvite} from '../_models/trip.interface';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {UserInterface} from '../_models/user.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getTrips(): Observable<TripInterface[]> {
    return this.http.get<TripInterface[]>(`${apiUrl}/trips`);
  }

  getImages(tripId: number): Observable<ImageInterface[]> {
    return this.http.get<any[]>(`${apiUrl}/images?tripId=${tripId}`).pipe(map((images) => {
      for (const image of images) {
        image.dateString = image.date;
        image.date = new Date(image.date);
      }
      return images;
    }));
  }

  addTrip(name: string, description?: string): Observable<TripInterface> {
    return this.http.post<TripInterface>(`${apiUrl}/trip`, {name, description});
  }

  addImages(tripId: number, metaDataArray: [{location: string, date: Date}], images: [File]): Observable<any> {
    const formData = new FormData();

    for (const image of images) {
      formData.append('images', image, image.name);
    }

    for (const metaData of metaDataArray) {
      const date = metaData.date.getFullYear() + '-' + (metaData.date.getMonth() + 1) + '-' + metaData.date.getDate();
      formData.append('metadata', JSON.stringify({
        date,
        location: metaData.location
      }));
    }

    formData.append('tripId', tripId.toString());

    return this.http.post<any>(`${apiUrl}/images`, formData);
  }

  deleteImage(name: string): Observable<any> {
    return this.http.delete<any>(`${apiUrl}/image?name=${name}`);
  }

  editImage(name: string, date: Date, location: string): Observable<any> {
    const dateString = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate();
    return this.http.patch<any>(`${apiUrl}/image`, {name, date: dateString, location});
  }

  deleteTrip(id: number): Observable<any> {
    return this.http.delete<any>(`${apiUrl}/trip?id=${id}`);
  }

  editTrip(id: number, name: string, description: string): Observable<any> {
    return this.http.patch<any>(`${apiUrl}/trip`, {id, name, description});
  }

  getUser(id: number): Observable<UserInterface> {
    return this.http.get<UserInterface>(`${apiUrl}/user?userId=${id}`);
  }

  getUsers(): Observable<UserInterface[]> {
    return this.http.get<UserInterface[]>(`${apiUrl}/users`);
  }

  getTripUsers(id: number): Observable<UserInterface[]> {
    return this.http.get<UserInterface[]>(`${apiUrl}/tripUsers?tripId=${id}`);
  }

  getInvites(): Observable<TripInvite[]> {
      return this.http.get<TripInvite[]>(`${apiUrl}/invites`);
  }

  inviteUserToTrip(tripId: number, user: number): Observable<any> {
    return this.http.post<any>(`${apiUrl}/invite`, {
      tripId,
      user
    });
  }

  acceptInvite(tripId: number): Observable<any> {
    return this.http.post<any>(`${apiUrl}/acceptInvite`, {
      tripId
    });
  }

  rejectInvite(tripId: number): Observable<any> {
    return this.http.post<any>(`${apiUrl}/rejectInvite`, {
      tripId
    });
  }

  leaveTrip(tripId: number): Observable<any> {
    return this.http.delete<any>(`${apiUrl}/leaveTrip?tripId=${tripId}`);
  }
}
