const db = require('./database');
const images = require('./images.api')

function getAll(req, res) {
  db.trips.all('SELECT id, name, description, user FROM trips WHERE user = ?',
    req.user.id,
    function (err, rows) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      db.trips.all(
        'SELECT trips.id, trips.name, trips.description, trips.user ' +
        'FROM tripsUser ' +
        'INNER JOIN trips ON tripsUser.trip=trips.id ' +
        'WHERE tripsUser.user = ?',
        req.user.id,
        function (err, additionalTrips) {
          if (err) {
            res.status(400).json({
              error: err.message
            });
            return;
          }
          res.json(rows.concat(additionalTrips));
        })
    })
}

function getTrip(req, res) {
  if (!req.query.tripId) {
    res.status(400).json({
      error: 'Bad params'
    });
    return;
  }
  db.trips.get(
    'SELECT * FROM tripsUser ' +
    'WHERE user = ? AND trip = ?',
    [req.user.id, req.query.tripId],
    function (err, row) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      if (!row) {
        db.trips.get(
          'SELECT id, name, description, user FROM trips WHERE user = ? AND id = ?',
          [req.user.id, req.query.tripId],
          function (err, row) {
            if (err) {
              res.status(400).json({
                error: err.message
              });
              return;
            }
            res.json(row);
          })
      }
      else {
        db.trips.get(
          'SELECT id, name, description, user FROM trips WHERE id = ?',
          req.query.tripId,
          function (err, row) {
            if (err) {
              res.status(400).json({
                error: err.message
              });
              return;
            }
            res.json(row);
          })
      }
    }
  )
}

function addTrip(req, res) {
  if (!req.body.name) {
    res.status(400).json({
      error: 'Not enough fields'
    });
    return;
  }
  if (typeof req.body.name !== "string") {
    res.status(400).json({
      error: 'Incorrect data format'
    });
    return;
  }
  db.trips.run('INSERT INTO trips (name, description, user) VALUES (?,?,?)',
    [req.body.name, req.body.description ? req.body.description : null, req.user.id],
    function (err) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      res.json({
        id: this.lastID,
        user: req.user.id,
        images: [],
        name: req.body.name,
        description: req.body.description ? req.body.description : null
      });
    })
}

function updateTrip(req, res) {
  db.trips.run('UPDATE trips SET ' +
    'name = COALESCE(?, name), ' +
    'description = COALESCE(?, description) ' +
    'WHERE id = ? AND user = ?',
    [req.body.name, req.body.description, req.body.id, req.user.id],
    function (err) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      res.json({
        message: 'Success'
      });
    }
  )
}

function deleteTrip(req, res) {
  db.trips.all(
    'SELECT name FROM images WHERE trip = ? AND user = ?',
    [req.query.id, req.user.id],
    async function (err, rows) {
      if (err) {
        res.status(400).json({
          err: err.message
        });
        return;
      }
      for (const name of rows) {
        await images.deleteImage(name.name, req.user.id).then(() => {}, (error) => {console.error(error)});
      }
      db.trips.run(
        'DELETE FROM trips WHERE id = ? AND user = ?',
        [req.query.id, req.user.id],
        function (err) {
          if (err) {
            res.status(400).json({
              error: err.message
            });
            return;
          }
          db.trips.run(
            'DELETE FROM tripsUser WHERE trip = ?',
            req.query.id,
            function (err) {
              if (err) {
                res.status(400).json({
                  error: err.message
                });
                return;
              }
              db.trips.run(
                'DELETE FROM tripInvites WHERE trip = ?',
                req.query.id,
                function (err) {
                  if (err) {
                    res.status(400).json({
                      error: err.message
                    });
                    return;
                  }
                  res.json({
                    message: 'Success'
                  });
                }
              )
            }
          )
        }
      )
    }
  )
}

function inviteUser(req, res) {
  db.trips.get(
    'SELECT user FROM trips WHERE id = ?',
    req.body.tripId,
    function (err, row) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      if (row.user === req.user.id) {
        db.trips.run(
          'INSERT INTO tripInvites ' +
          '(user, trip, owner) ' +
          'VALUES (?,?,?)',
          [req.body.user, req.body.tripId, req.user.id],
          function (err) {
            if (err) {
              res.status(400).json({
                error: err.message
              });
              return;
            }
            res.json({});
          });
      }
      else {
        res.status(400).json({
          error: 'Not your trip'
        });
      }
    });
}

function getInvites(req, res) {
  db.trips.all(
    'SELECT name, trip, trips.user AS user ' +
    'FROM tripInvites ' +
    'INNER JOIN trips ON tripInvites.trip=trips.id ' +
    'WHERE tripInvites.user = ? AND tripInvites.owner=trips.user',
    req.user.id,
    function (err, rows) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      res.json(rows);
    })
}

function acceptInvite(req, res) {
  db.trips.run(
    'DELETE FROM tripInvites WHERE user = ? AND trip = ?',
    [req.user.id, req.body.tripId],
    function (err) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      if (!this.changes) {
        res.json({
          message: 'No invite'
        });
        return;
      }
      db.trips.run(
        'INSERT INTO tripsUser ' +
        '(user, trip) ' +
        'VALUES (?,?)',
        [req.user.id, req.body.tripId],
        function (err) {
          if (err) {
            res.status(400).json({
              error: err.message
            });
            return;
          }
          res.json({});
        }
      )
    }
  )
}

function rejectInvite(req, res) {
  db.trips.run(
    'DELETE FROM tripInvites WHERE user = ? AND trip = ?',
    [req.user.id, req.body.tripId],
    function (err) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      if (!this.changes) {
        res.json({
          message: 'No invite'
        });
        return;
      }
      res.json({});
    }
  )
}

function getUsers(req, res) {
  db.trips.get(
    'SELECT user FROM trips WHERE id = ?',
    req.query.tripId,
    function (err, row) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      db.user.get(
        'SELECT id, username FROM user WHERE id = ?',
        row.user,
        function (err, row) {
          if (err) {
            res.status(400).json({
              error: err.message
            });
            return;
          }
          const users = [row];
          db.trips.all(
            'SELECT user FROM tripsUser WHERE trip = ?',
            req.query.tripId,
            function (err, rows) {
              if (err) {
                res.status(400).json({
                  error: err.message
                });
                return;
              }
              const userIds = [];
              for (const row of rows) {
                userIds.push(row.user);
              }
              db.user.all(
                'SELECT id, username FROM user WHERE id IN (' + userIds.join(',') + ')',
                function (err, rows) {
                  if (err) {
                    res.status(400).json({
                      error: err.message
                    });
                    return;
                  }
                  res.json(users.concat(rows));
                }
              )
            }
          )
        }
      )
    }
  )
}

function leaveTrip(req, res) {
  db.trips.run(
    'DELETE FROM tripsUser WHERE user = ? AND trip = ?',
    [req.user.id, req.query.tripId],
    function (err) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      res.json();
    }
  )
}

module.exports = {
  getAll,
  getTrip,
  addTrip,
  updateTrip,
  deleteTrip,
  inviteUser,
  getInvites,
  acceptInvite,
  rejectInvite,
  getUsers,
  leaveTrip,
}
