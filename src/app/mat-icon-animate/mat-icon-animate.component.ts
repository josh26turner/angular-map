import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-mat-icon-animate',
  templateUrl: './mat-icon-animate.component.html',
  styleUrls: ['./mat-icon-animate.component.scss']
})
export class MatIconAnimateComponent {
  @Input() mouseInIcon: String;
  @Input() mouseOutIcon: String;

  public mouseIn: boolean;

  constructor() { }
}
