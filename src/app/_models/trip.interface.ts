export interface TripInterface {
  id: number;
  name: string;
  description: string;
  images?: ImageInterface[];
  user: number;
}

export interface ImageInterface {
  name: string;
  dateString: string;
  location: string;
  date: Date;
  user: number;
}

export interface TripInvite {
  trip: number;
  name: string;
  user: number;
}

export enum TripChange {
  Deleted,
  Edited,
}
