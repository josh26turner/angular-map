import {Component, Input, OnInit} from '@angular/core';
import {TripInterface, TripChange} from '../_models/trip.interface';
import {ApiService} from '../_services/api.service';
import {imageUrl} from '../config';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {AddTripComponent} from '../add-trip/add-trip.component';
import {AddImagesComponent} from '../add-image/add-images.component';
import {ViewImageComponent} from '../view-image/view-image.component';
import {MapComponent} from '../map/map.component';
import {ViewTripComponent} from '../view-trip/view-trip.component';
import { ConfirmationComponent } from '../confirmation/confirmation.component';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-trips',
  templateUrl: './trips.component.html',
  styleUrls: ['./trips.component.scss']
})
export class TripsComponent implements OnInit {
  @Input()
  country: string;
  @Input()
  admin: string;

  @Input()
  trips: TripInterface[];
  imageUrl = imageUrl;
  addTripForm: FormGroup;

  constructor(private apiService: ApiService,
              private formBuilder: FormBuilder,
              private dialog: MatDialog,
              private mapComponent: MapComponent) {
    this.addTripForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  async ngOnInit() {
    const invites = await this.apiService.getInvites().toPromise();
    let accepted = false;

    for (const invite of invites) {
      const matDialogConfig = new MatDialogConfig();
      matDialogConfig.data = {
        question: `Accept invite to ${invite.name} from ${(await this.apiService.getUser(invite.user).toPromise()).username}?`
      };
      const dialogRef = this.dialog.open(ConfirmationComponent, matDialogConfig);
      await new Observable((resolve) => {
        dialogRef.afterClosed().subscribe(async res => {
          if (res) {
            await this.apiService.acceptInvite(invite.trip).toPromise();
            accepted = true;
            resolve.complete();
          }
          else {
            await this.apiService.rejectInvite(invite.trip).toPromise();
            resolve.complete();
          }
        });
      }).toPromise();
    }

    if (accepted) {
      await this.mapComponent.refreshTrips();
    }
  }

  addTripDialog() {
    const dialogRef = this.dialog.open(AddTripComponent);
    dialogRef.afterClosed().subscribe(async (trip: TripInterface) => {
      if (trip) {
        this.trips.push(trip);
        this.mapComponent.sortTrips();
      }
    });
  }

  addImageDialog(trip: TripInterface) {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      trip
    };
    if (trip.images.length > 0) {
      matDialogConfig.data.fill = {
        location: trip.images[0].location,
        date: trip.images[0].date
      };
    }
    const dialogRef = this.dialog.open(AddImagesComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(async images => {
      if (images) {
        trip.images = trip.images.concat(images);
        await this.mapComponent.drawChart();
        this.mapComponent.sortTrips();
      }
    });
  }

  viewImageDialog(trip: TripInterface, index: number) {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      images: trip.images,
      index,
      map: this.mapComponent
    };
    const dialogRef = this.dialog.open(ViewImageComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(deleted => {
      if (deleted) {
        trip.images.splice(index, 1);
        this.mapComponent.drawChart();
      }
    });
  }

  containsLocation(trip: TripInterface, location: string): boolean {
    if (location === 'World') {
      return true;
    }
    if (trip.images.length === 0) {
      return true;
    }
    for (const image of trip.images) {
      if (image.location === location) {
        return true;
      }
    }
    return false;
  }

  viewTripDialog(event: any, trip: TripInterface) {
    event.stopPropagation();

    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      trip,
      map: this.mapComponent
    };
    const dialogRef = this.dialog.open(ViewTripComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(async changed => {
      if (changed === TripChange.Edited) {
        await this.mapComponent.refreshTrips();
        await this.mapComponent.drawChart();
        this.mapComponent.sortTrips();
      }
      else if (changed === TripChange.Deleted) {
        this.trips.splice(this.trips.indexOf(trip), 1);
      }
    });
  }
}
