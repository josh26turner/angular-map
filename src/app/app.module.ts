import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MapComponent } from './map/map.component';
import {RouterModule} from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatCardModule} from '@angular/material/card';
import { LoginComponent } from './login/login.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {ReactiveFormsModule} from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import {JwtInterceptor} from './_services/jwt.interceptor';
import { ProfileComponent } from './profile/profile.component';
import {AuthGuard} from './_services/auth.guard';
import { TripsComponent } from './trips/trips.component';
import {MatIconModule} from '@angular/material/icon';
import { AddTripComponent } from './add-trip/add-trip.component';
import {MatDialogModule} from '@angular/material/dialog';
import { AddImagesComponent } from './add-image/add-images.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MaterialFileInputModule} from 'ngx-material-file-input';
import {MatExpansionModule} from '@angular/material/expansion';
import { ViewImageComponent } from './view-image/view-image.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { EditImageComponent } from './edit-image/edit-image.component';
import { EditTripComponent } from './edit-trip/edit-trip.component';
import { ViewTripComponent } from './view-trip/view-trip.component';
import { IndexComponent } from './index/index.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatListModule} from '@angular/material/list';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import { InviteUserComponent } from './invite-user/invite-user.component';
import { AlertComponent } from './_components/alert.component';
import {HttpErrorInterceptor} from './_services/http-error.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { MatIconAnimateComponent } from './mat-icon-animate/mat-icon-animate.component';

const ROUTES = [
  {
    path: 'map',
    component: MapComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: '',
    component: IndexComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    MapComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    TripsComponent,
    AddTripComponent,
    AddImagesComponent,
    ViewImageComponent,
    ConfirmationComponent,
    EditImageComponent,
    EditTripComponent,
    ViewTripComponent,
    IndexComponent,
    InviteUserComponent,
    AlertComponent,
    MatIconAnimateComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(ROUTES),
    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    HttpClientModule,
    MatIconModule,
    MatDialogModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MaterialFileInputModule,
    MatExpansionModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatToolbarModule,
    MatListModule,
    FlexLayoutModule,
    MatProgressSpinnerModule,
    MatAutocompleteModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
