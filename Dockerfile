FROM node:12-alpine
WORKDIR /usr/src/app

COPY . .

RUN npm install
RUN npm run build-prod

FROM node:12-alpine
WORKDIR /usr/src/app

COPY package.json .

RUN npm install --production

FROM node:12-alpine
WORKDIR /usr/src/app

COPY server.js .
COPY package.json .
COPY ./api ./api

COPY --from=0 /usr/src/app/dist ./dist
COPY --from=1 /usr/src/app/node_modules ./node_modules

EXPOSE 3000

CMD ["node", "server.js"]
