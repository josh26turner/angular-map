IMAGE_TAG=${CI_COMMIT_TAG:-$CI_COMMIT_REF_SLUG}

docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
docker pull $CI_REGISTRY_IMAGE:latest || true
docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:$IMAGE_TAG --tag $CI_REGISTRY_IMAGE:latest .
docker push $CI_REGISTRY_IMAGE:$IMAGE_TAG
docker push $CI_REGISTRY_IMAGE:latest

if [ ! -z "$CI_COMMIT_TAG" ]
then
  docker tag $CI_REGISTRY_IMAGE:latest $DOCKERHUB_IMAGE:$CI_COMMIT_TAG
  docker tag $CI_REGISTRY_IMAGE:latest $DOCKERHUB_IMAGE:latest
  docker login -u $DOCKERHUB_USER -p $DOCKERHUB_PASS
  docker push $DOCKERHUB_IMAGE:latest
  docker push $DOCKERHUB_IMAGE:$CI_COMMIT_TAG
fi

