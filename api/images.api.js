const fs = require('fs')
const sharp = require('sharp')

const db = require('./database')

const imagePath = require('./config').imagePath

function getAll(req, res) {
  if (!req.query.tripId) {
    res.status(400).json({
      error: 'Bad params'
    });
    return;
  }
  try {
    const tripId = parseInt(req.query.tripId);
    db.trips.get(
      'SELECT * FROM trips WHERE id = ? AND user = ?',
      [tripId, req.user.id],
      function (err, row) {
        if (err) {
          res.status(400).json({
            error: err.message
          });
        }
        if (!row) {
          db.trips.get(
          'SELECT * FROM tripsUser ' +
          'WHERE user = ? AND trip = ?',
          [req.user.id, req.query.tripId],
          function (err, row) {
            if (err) {
              res.status(400).json({
                error: err.message
              });
              return;
            }
            if (!row) {
              res.status(400).json({
                error: 'No trip'
              });
              return;
            }
            db.trips.all(
              'SELECT name, date, location, user FROM images WHERE trip = ?',
              tripId,
              function (err, rows) {
                if (err) {
                  console.error(err);
                  res.status(400).json({
                    error: err.message
                  });
                  return;
                }
                res.json(rows);
              });
          })
        }
        else {
          db.trips.all(
            'SELECT name, date, location, user FROM images WHERE trip = ?',
            tripId,
            function (err, rows) {
              if (err) {
                console.error(err);
                res.status(400).json({
                  error: err.message
                });
                return;
              }
              res.json(rows);
            });
        }
      }
    );
  }
  catch (e) {
    res.status(400).json({
      error: e.message
    });
  }
}

function addImages(req, res) {
  db.trips.get('SELECT * FROM trips WHERE id = ?', req.body.tripId, async function (err, row) {
    if (err) {
      res.status(400).json({
        error: err.message
      });
      return;
    }
    if (!row) {
      res.status(400).json({
        error: 'No such trip'
      });
      return;
    }

    const images = [];
    if (Array.isArray(req.files.images) && Array.isArray(req.body.metadata)) {
      if (req.files.images.length !== req.body.metadata.length) {
        res.status(400).json({
          error: 'Unequal metadata and images arrays'
        });
        return;
      }

      for (let i = 0; i < req.files.images.length; i ++) {
        const metadata = JSON.parse(req.body.metadata[i]);
        images.push(await processImage(
          req.files.images[i],
          req.user.id,
          metadata.date,
          metadata.location,
          req.body.tripId));
      }
    }
    else {
      const image = req.files.images;
      const metadata = JSON.parse(req.body.metadata);
      images.push(await processImage(image, req.user.id, metadata.date, metadata.location, req.body.tripId));
    }
    for (let image of images) {
      image.user = req.user.id;
    }
    res.json(images);
  });
}

function updateImage(req, res) {
  db.trips.run('UPDATE images SET ' +
    'date = COALESCE(?, date), ' +
    'location = COALESCE(?, location) ' +
    'WHERE name = ? AND user = ?',
    [req.body.date, req.body.location, req.body.name, req.user.id],
    function (err) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      res.json({
        message: 'Success'
      });
    }
  )
}

async function removeImage(req, res) {
  await deleteImage(req.query.name, req.user.id)
    .then(
      success => {
        res.json({
          message: success
        })},
      failure => {
        res.status(400).json({
          error: failure
        })
      }
    );
}

function deleteImage(name, id) {
  return new Promise((resolve, reject) => db.trips.run(
    'DELETE FROM images WHERE name = ? AND user = ?',
    [name, id],
    function (err) {
      if (err) {
        reject(err.message);
        return;
      }
      fs.unlink(imagePath + name, err => {
        if (err) {
          reject('Failed to delete image');
        }
        resolve('Success');
      });
    }
  ));
}

function processImage(image, user, date, location, trip) {
  return new Promise(async (resolve, reject) => {
    let name;
    do {
      name = makeId(30) + '.jpeg';
    } while(fs.existsSync(imagePath + name));

    await sharp(image.data, {failOnError: false})
      .rotate()
      .resize(1000, 1000, {withoutEnlargement: true, fit: 'inside'})
      .toFormat('jpeg')
      .toFile(imagePath + name);
    db.trips.run(
      'INSERT INTO images (user,name,date,location,trip) VALUES (?,?,?,?,?)',
      [user, name, date, location, trip],
      function (err) {
        if (err) {
          reject(err.message);
          return;
        }
        resolve({
          id: this.lastID,
          name: name,
          date: date,
          location: location
        });
      }
    )
  })
}

function makeId(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++ ) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

module.exports = {
  getAll,
  addImages,
  updateImage,
  removeImage,
  deleteImage
}
