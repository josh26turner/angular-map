const docker = true;

module.exports = {
  imagePath: docker ? '/images/' : './images/',
  configPath: docker ? '/config/' : './'
}
