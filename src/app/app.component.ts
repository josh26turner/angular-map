import { Component } from '@angular/core';
import {AuthenticationService} from './_services/authentication.service';
import {UserInterface} from './_models/user.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Angular Map';
  user: UserInterface;

  constructor(private authenticationService: AuthenticationService) {
    this.user = this.authenticationService.currentUserValue;
  }

  logout(){
    this.authenticationService.logout();
    window.location.href = '/';
  }
}
