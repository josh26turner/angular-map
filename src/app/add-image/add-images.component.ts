import {Component, Inject, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ImageInterface, TripInterface} from '../_models/trip.interface';
import {ApiService} from '../_services/api.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {locations, filter, locationValidator} from '../_models/locations.list';
import {gps, parse} from 'exifr';
import {geoContains, geoPath, geoArea} from 'd3-geo';
import * as topojson from 'topojson-client';
import * as features from '../../assets/features-simplified.json';

@Component({
  selector: 'app-add-image',
  templateUrl: './add-images.component.html',
  styleUrls: ['./add-images.component.css']
})
export class AddImagesComponent implements OnInit {
  addImagesForm: FormGroup;
  locationsObservables: Observable<string[]>[];
  locations: string[];
  trip: TripInterface;
  loading: boolean;
  geojson: any;

  imageSrcs: string[];

  private readonly fillData: any;

  constructor(private formBuilder: FormBuilder,
              private apiService: ApiService,
              private dialogRef: MatDialogRef<AddImagesComponent>,
              @Inject(MAT_DIALOG_DATA) data) {
    this.trip = data.trip;

    this.locations = locations;

    if (data.fill) {
      this.fillData = data.fill;
    }
    else {
      this.fillData = null;
    }
  }

  ngOnInit(): void {
    this.addImagesForm = this.formBuilder.group({
      images: ['', Validators.required],
      imageMetaData: this.formBuilder.array(
        [],
        [Validators.required]
      )
    });
    this.locationsObservables = [];

    this.addImagesForm.get('images').valueChanges.subscribe(x => {
      if (x !== null) {
        this.readURLs(x.files);
        this.onChangeImages(x.files.length);
        this.readExifData(x.files);
      }
      else {
        this.imageSrcs = [];
        this.onChangeImages(0);
      }
    });

    // @ts-ignore
    this.geojson = topojson.feature(features.default, features.default.objects.features);
  }

  get f() { return this.addImagesForm.controls; }
  get imageMetaDataArray() { return this.f.imageMetaData as FormArray; }

  addImage(){
    if (this.addImagesForm.invalid) {
      return;
    }
    this.loading = true;
    this.apiService.addImages(
      this.trip.id,
      this.addImagesForm.value.imageMetaData,
      this.addImagesForm.value.images.files).subscribe((images: ImageInterface[]) => {
        console.log(images);
        for (let image of images) {
          // @ts-ignore
          image.dateString = image.date;
          image.date = new Date(image.dateString);
          console.log(image);
        }
        this.dialogRef.close(images);
    });
  }

  onChangeImages(numberOfImages: number) {
    if (this.imageMetaDataArray.length < numberOfImages) {
      for (let i = this.imageMetaDataArray.length; i < numberOfImages; i ++) {
        this.imageMetaDataArray.push(this.formBuilder.group({
          date: [this.fillData ? this.fillData.date : '', // Get from exif data of image
            Validators.required],
          location: new FormControl(this.fillData ? this.fillData.location : '', // Get from exif data of image
            [locationValidator()]),
        }));
        this.locationsObservables.push(this.imageMetaDataArray.at(i).get('location').valueChanges
          .pipe(
            startWith(''),
            map(value => filter(value))
          ));
      }
    }
    else {
      for (let i = this.imageMetaDataArray.length; i >= numberOfImages; i--) {
        this.imageMetaDataArray.removeAt(i);
      }
      this.locationsObservables.length = numberOfImages;
    }
  }

  readURLs(images: [File]) {
    this.imageSrcs = new Array<string>(images.length);
    images.forEach(((value, index) => {
      const reader = new FileReader();

      reader.onload = e => {
        if (typeof e.target.result === 'string') {
          this.imageSrcs[index] = e.target.result;
        }
      };

      reader.readAsDataURL(value);
    }));
  }

  readExifData(images: [File]) {
    images.forEach((value, index) => {
      parse(value).then(res => {
        if (res) {
          if (res.DateTimeOriginal) {
            (this.imageMetaDataArray.at(index) as FormGroup).controls.date.setValue(res.DateTimeOriginal);
          }
          if ((res.longitude && res.latitude) && (!isNaN(res.longitude) && !isNaN(res.latitude))) {
            for (const feature of this.geojson.features) {
              if (geoContains(feature, [res.longitude, res.latitude])) {
                (this.imageMetaDataArray.at(index) as FormGroup).controls.location.setValue(feature.properties.name);
              }
            }
          }
        }
      });
    });
  }

  propagateLocations(index: number) {
    const location = (this.imageMetaDataArray.at(index) as FormGroup).value.location;
    for (let i = index + 1; i < this.imageMetaDataArray.length; i ++) {
      (this.imageMetaDataArray.at(i) as FormGroup).controls.location.setValue(location);
    }
  }

  propagateDates(index: number) {
    const date = (this.imageMetaDataArray.at(index) as FormGroup).value.date;
    for (let i = index + 1; i < this.imageMetaDataArray.length; i ++) {
      (this.imageMetaDataArray.at(i) as FormGroup).controls.date.setValue(date);
    }
  }
}
