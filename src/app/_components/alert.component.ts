import { Component, OnInit } from '@angular/core';
import {AlertService} from '../_services/alert.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-alert',
  template: '',
})
export class AlertComponent implements OnInit {
  private subscription: Subscription;
  message: any;

  constructor(private alertService: AlertService,
              private snackbar: MatSnackBar) {}

  ngOnInit(): void {
    this.subscription = this.alertService.getAlert().subscribe((message) => {
      switch (message && message.type) {
        case 'success':
          this.snackbar.open(message.text, 'Dismiss', {
            duration: 3000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
          break;
        case 'error':
          message.cssClass = 'alert alert-danger';
          this.snackbar.open(message.text, 'Dismiss', {
            duration: 10000,
            horizontalPosition: 'end',
            verticalPosition: 'bottom'
          });
          break;
      }
    });
  }

}
