import {Component, HostListener, Inject, OnInit} from '@angular/core';
import {ImageInterface} from '../_models/trip.interface';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {imageUrl} from '../config';
import {ConfirmationComponent} from '../confirmation/confirmation.component';
import {ApiService} from '../_services/api.service';
import {EditImageComponent} from '../edit-image/edit-image.component';
import {AuthenticationService} from '../_services/authentication.service';
import {UserInterface} from '../_models/user.interface';
import { MapComponent } from '../map/map.component';

@Component({
  selector: 'app-view-image',
  templateUrl: './view-image.component.html',
  styleUrls: ['./view-image.component.css']
})
export class ViewImageComponent implements OnInit {
  images: ImageInterface[];
  index: number;
  imageUrl = imageUrl;

  user: UserInterface;

  private dialogOpen = false;
  private mapComponent: MapComponent;

  constructor(private dialogRef: MatDialogRef<ViewImageComponent>,
              @Inject(MAT_DIALOG_DATA) data,
              private dialog: MatDialog,
              private apiService: ApiService,
              authenticationService: AuthenticationService) {
    this.user = authenticationService.currentUserValue;
    this.images = data.images;
    this.index = data.index;
    this.mapComponent = data.map;
  }

  ngOnInit(): void {
  }

  @HostListener('window:keyup', ['$event'])
  keyUp(event: KeyboardEvent) {
    if (this.dialogOpen) {
      return;
    }
    if (event.key === 'ArrowRight') {
      this.index = (this.index + 1) % this.images.length;
    }
    else if (event.key === 'ArrowLeft') {
      this.index = (this.index - 1 + this.images.length) % this.images.length;
    }
  }

  editImageDialog() {
    this.dialogOpen = true;
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      image: this.images[this.index]
    };
    const dialogRef = this.dialog.open(EditImageComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(_ => {
      this.dialogOpen = false;
      this.mapComponent.drawChart();
    });
  }

  deleteImageDialog() {
    this.dialogOpen = true;
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      question: 'Are you sure?'
    };
    const dialogRef = this.dialog.open(ConfirmationComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(async res => {
      this.dialogOpen = false;
      if (res) {
        await this.apiService.deleteImage(this.images[this.index].name).toPromise();
        this.dialogRef.close(true);
      }
    });
  }
}
