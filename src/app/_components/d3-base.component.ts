import {AfterViewInit, ElementRef, OnChanges, OnDestroy, SimpleChange, ViewChild} from '@angular/core';

import {select, Selection} from 'd3';
import {fromEvent, Observable, Subscription} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

export interface Margin {
  top: number;
  right: number;
  bottom: number;
  left: number;
}

export abstract class D3BaseComponent implements AfterViewInit, OnDestroy, OnChanges {
  @ViewChild('chartWrapper') chartWrapperRef: ElementRef<HTMLDivElement>;
  @ViewChild('chart') chartRef: ElementRef<HTMLDivElement>;

  margin: Margin = {top: 0, right: 0, bottom: 0, left: 0};
  width = 0;
  height = 0;

  chart: Selection<any, any, any, any>;
  svg: Selection<any, any, any, any>;

  resize$: Observable<Window>;
  resize$Subscription: Subscription;

  ngAfterViewInit(): void {
    this.svg = select(this.chartRef.nativeElement).append('svg');
    this.chart = this.svg
      .append('g')
      .attr('transform', `translate(${this.margin.left}, ${this.margin.top})`);

    this.resize$ = fromEvent<Window>(window, 'resize')
      .pipe(
        debounceTime(300)
      );

    this.resize$Subscription = this.resize$.subscribe(() => {
      this.resize();
      this.drawChart();
    });

    this.resize();
    this.init();
    this.drawChart();
  }

  ngOnDestroy(): void {
    this.resize$Subscription.unsubscribe();
  }

  ngOnChanges(changes: {[propName: string]: SimpleChange}): void {
    if (this.chart) {
      this.drawChart();
    }
  }

  protected resize(): void {
    this.width = this.chartWrapperRef.nativeElement.clientWidth - this.margin.left - this.margin.right;
    this.height = this.chartWrapperRef.nativeElement.clientHeight - this.margin.top - this.margin.bottom;
    this.svg
      .attr('width', this.width + this.margin.left + this.margin.right)
      .attr('height', this.height + this.margin.top + this.margin.bottom);

  }

  abstract init(): void;

  abstract drawChart(): void;
}
