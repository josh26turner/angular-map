const express = require("express");
const path = require("path");
const http = require("http");
const bodyParser = require("body-parser");

const api = require("./api/api");

const imagePath = require('./api/config').imagePath

const cors = require("cors")

const app = express();

const corsOptions = {
  origin: "http://localhost:4200",
  optionsSuccessStatus: 200,
};

app
  .use(bodyParser.urlencoded({ extended: false }))
  .use(bodyParser.json())
  .use(cors(corsOptions))
  .use(express.static(path.join(__dirname, "dist/papa")))
  .use("/images", express.static(imagePath))
  .use("/api", api)

// Catch all other routes and return the index file
app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "dist/papa/index.html"));
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || 3000;
app.set("port", port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`Serving on http://localhost:${port}`));
