const debug = false;
export const apiUrl = (debug ? 'http://localhost:3000' : '') + '/api';
export const imageUrl = (debug ? 'http://localhost:3000' : '') + '/images';
