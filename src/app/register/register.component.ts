import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../_services/authentication.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registrationForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthenticationService) {
    this.registrationForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required],
      passwordCheck: ['', Validators.required]
    }, {validator: this.passwordCheck('password', 'passwordCheck')});
  }

  ngOnInit(): void {
    if (this.authService.currentUserValue && this.authService.currentUserValue.token) {
      window.location.href = '/map';
    }
  }

  register(){
    if (this.registrationForm.invalid) {
      return;
    }
    this.authService.register(this.registrationForm.value.username,
      this.registrationForm.value.password)
      .subscribe(() => {
        window.location.href = '/map';
    });
  }

  passwordCheck(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ mustMatch: true });
      } else {
        matchingControl.setErrors(null);
      }
    };
  }
}
