import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogConfig, MatDialogRef} from '@angular/material/dialog';
import {TripInterface, TripChange} from '../_models/trip.interface';
import {imageUrl} from '../config';
import {ViewImageComponent} from '../view-image/view-image.component';
import {AddImagesComponent} from '../add-image/add-images.component';
import {EditTripComponent} from '../edit-trip/edit-trip.component';
import {ConfirmationComponent} from '../confirmation/confirmation.component';
import {ApiService} from '../_services/api.service';
import {AuthenticationService} from '../_services/authentication.service';
import {UserInterface} from '../_models/user.interface';
import {InviteUserComponent} from '../invite-user/invite-user.component';
import {MapComponent} from '../map/map.component';

@Component({
  selector: 'app-view-trip',
  templateUrl: './view-trip.component.html',
  styleUrls: ['./view-trip.component.scss']
})
export class ViewTripComponent implements OnInit {
  trip: TripInterface;
  imageUrl = imageUrl;
  user: UserInterface;
  users: UserInterface[] = [];

  private mapComponent: MapComponent;

  constructor(private dialogRef: MatDialogRef<ViewTripComponent>,
              @Inject(MAT_DIALOG_DATA) data,
              private dialog: MatDialog,
              private apiService: ApiService,
              authenticationService: AuthenticationService) {
    this.user = authenticationService.currentUserValue;
    this.trip = data.trip;
    this.mapComponent = data.map;
  }

  async ngOnInit() {
    this.users = await this.apiService.getTripUsers(this.trip.id).toPromise();
  }

  editTripDialog() {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      trip: this.trip
    };
    this.dialog.open(EditTripComponent, matDialogConfig);
  }

  deleteTripDialog() {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      question: `Delete trip ${this.trip.name}?`
    };
    const dialogRef = this.dialog.open(ConfirmationComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(async confirm => {
      if (confirm) {
        await this.apiService.deleteTrip(this.trip.id).toPromise();
        this.dialogRef.close(TripChange.Deleted);
      }
    });
  }

  leaveTripDialog() {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      question: `Leave trip ${this.trip.name}?`
    };
    const dialogRef = this.dialog.open(ConfirmationComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(async confirm => {
      if (confirm) {
        await this.apiService.leaveTrip(this.trip.id).toPromise();
        this.dialogRef.close(TripChange.Deleted);
      }
    });
  }

  viewImageDialog(trip: TripInterface, index: number) {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      images: trip.images,
      index,
      map: this.mapComponent
    };
    const dialogRef = this.dialog.open(ViewImageComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(deleted => {
      console.log(deleted);
      if (deleted) {
        trip.images.splice(index, 1);
        this.mapComponent.drawChart();
      }
    });
  }

  addImageDialog() {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      trip: this.trip
    };
    if (this.trip.images.length > 0) {
      matDialogConfig.data.fill = {
        location: this.trip.images[0].location,
        date: this.trip.images[0].date
      };
    }
    const dialogRef = this.dialog.open(AddImagesComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(async images => {
      if (images) {
        this.trip.images = this.trip.images.concat(images);
        this.mapComponent.drawChart();
      }
    });
  }

  async inviteUsersDialog() {
    const matDialogConfig = new MatDialogConfig();
    const userIds = this.users.map(user => user.id);
    const nonMemberUsers = (await this.apiService.getUsers().toPromise()).filter(user => !userIds.includes(user.id));
    matDialogConfig.data = {
      trip: this.trip,
      users: nonMemberUsers
    };
    this.dialog.open(InviteUserComponent, matDialogConfig);
  }

  usersToString(users: UserInterface[]): string {
    return users.map(user => user.username).join(', ');
  }
}
