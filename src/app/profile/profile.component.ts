import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserInterface} from '../_models/user.interface';
import {AuthenticationService} from '../_services/authentication.service';
import {Router} from '@angular/router';
import {MatDialog, MatDialogConfig} from '@angular/material/dialog';
import {ConfirmationComponent} from '../confirmation/confirmation.component';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  updateForm: FormGroup;
  currentUser: UserInterface;

  constructor(private authenticationService: AuthenticationService,
              private formBuilder: FormBuilder,
              private router: Router,
              private dialog: MatDialog) {
    this.currentUser = this.authenticationService.currentUserValue;
    this.updateForm = this.formBuilder.group({
      username: [this.currentUser.username, Validators.required],
      password: ['']
    });
  }

  ngOnInit(): void {
  }

  updateProfile() {
    if (this.updateForm.invalid) {
      return;
    }

    this.authenticationService.update(this.updateForm.value.username, this.updateForm.value.password)
      .subscribe();
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
  }

  delete() {
    const matDialogConfig = new MatDialogConfig();
    matDialogConfig.data = {
      question: 'Delete your profile?'
    };
    const dialogRef = this.dialog.open(ConfirmationComponent, matDialogConfig);
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.authenticationService.delete().pipe().subscribe(() => {
          this.authenticationService.logout();
          this.router.navigate(['/']);
        });
      }
    });
  }
}
