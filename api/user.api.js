const db = require('./database');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const jwtSecret = process.env.JWT_SECRET;

function login(req, res) {
  db.user.get('SELECT * FROM user WHERE username = ?', [req.body.username], (err, row) => {
    if (err) {
      res.status(400).json({
        error: err.message
      });
      return;
    }
    if (!row){
      res.status(400).json({
        error: 'Invalid login'
      });
      return;
    }
    bcrypt.compare(req.body.password, row.password, (err, result) => {
      if (result) {
        let payload = {id: row.id};
        let token = jwt.sign(payload, jwtSecret);
        res.json({
          username: req.body.username,
          token: token,
          id: row.id
        });
      }
      else {
        res.status(400).json({
          error: 'Invalid login'
        });
      }
    });
  })
}

function register(req, res) {let errors = [];
  if (!req.body.password)
    errors.push('No password specified');
  if (!req.body.username)
    errors.push('No username specified');
  if (errors.length) {
    res.status(400).json({
      error: errors.join(', ')
    });
    return;
  }
  let data = {
    username: req.body.username,
    password: bcrypt.hashSync(req.body.password, 10)
  };
  db.user.run('INSERT INTO user (username, password) VALUES (?,?)',
    [data.username, data.password],
    function(err, row) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      res.json({
        username: req.body.username,
        token: jwt.sign({id: this.lastID}, jwtSecret),
        id: this.lastID
      });
    });
}

function update(req, res) {
  db.user.run('UPDATE user SET ' +
    'username = COALESCE(?, username), ' +
    'password = COALESCE(?, password) ' +
    'WHERE id = ?',
    [req.body.username, req.body.password ? bcrypt.hashSync(req.body.password, 10) : undefined, req.user.id],
    function (err) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return
      }
      db.user.get('SELECT username FROM user WHERE id = ?', req.user.id, function (err, row) {
        if (err) {
          res.status(400).json({
            error: err.message
          });
          return;
        }
        if (!row) {
          res.status(400).json({
            error: 'No user'
          });
          return;
        }
        res.json({
          token: jwt.sign({id: req.user.id}, jwtSecret),
          username: row.username
        });
      });
    })
}

function remove(req, res) {
  db.user.run('DELETE FROM user WHERE id = ?',
    req.user.id,
    function (err) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      res.json();
    })
}

function getUsers(req, res) {
  db.user.all(
    'SELECT id, username FROM user',
    [],
    function (err, rows) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      res.json(rows);
    }
  )
}

function getUser(req, res) {
  db.user.get(
    'SELECT id, username FROM user WHERE id = ?',
    req.query.userId,
    function (err, row) {
      if (err) {
        res.status(400).json({
          error: err.message
        });
        return;
      }
      res.json(row);
    }
  )
}

module.exports = {
  login,
  register,
  update,
  remove,
  getUsers,
  getUser
};
