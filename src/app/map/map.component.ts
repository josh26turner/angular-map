import {Component} from '@angular/core';
import {geoIdentity, geoPath, zoom, zoomIdentity, zoomTransform, interpolate, mouse, event} from 'd3';
import * as topojson from 'topojson-client';
import * as features from '../../assets/features-simplified.json';
import * as points from '../../assets/points-topo.json';
import {D3BaseComponent} from '../_components/d3-base.component';
import {TripInterface} from '../_models/trip.interface';
import {ApiService} from '../_services/api.service';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent extends D3BaseComponent {
  path: any;
  features: any;
  points: any;
  zoom: any;
  selectedCountry = 'World';
  title = 'World';
  admin = undefined;
  trips: TripInterface[];
  imageCount = 0;

  constructor(private apiService: ApiService) {
    super();
  }


  async init() {
    // @ts-ignore
    this.features = features.default;
    // @ts-ignore
    this.points = points.default;

    this.chart.append('rect').attr('id', 'background');
    this.chart.append('g').attr('id', 'fill');
    this.chart.append('path').attr('id', 'country');
    this.chart.append('g').attr('id', 'point');
  }

  async refreshTrips() {
    this.trips = await this.apiService.getTrips().toPromise();
    const promises = [];
    for (const trip of this.trips) {
      promises.push(this.apiService.getImages(trip.id).toPromise());
    }
    const images = await Promise.all(promises);
    for (let i = 0; i < images.length; i ++) {
      this.trips[i].images = images[i];
    }

    this.sortTrips();
  }

  sortTrips() {
    for (const trip of this.trips) {
      trip.images.sort((a, b) => (a.date < b.date) ? 1 : -1);
    }
    this.trips.sort((a, b) => {
      if (a.images.length === 0) {
        return -1;
      }
      if (b.images.length === 0) {
        return 1;
      }
      return (a.images[0].date < b.images[0].date) ? 1 : -1;
    });
  }

  frequencyLocation(trips: TripInterface[]) {
    const locationsCount = {};
    let max = 0;
    for (const trip of trips) {
      for (const image of trip.images) {
        if (locationsCount[image.location]) {
          locationsCount[image.location] ++;
        }
        else {
          locationsCount[image.location] = 1;
        }
        if (locationsCount[image.location] > max) {
          max = locationsCount[image.location];
        }
      }
    }
    let total = 0;
    for (const location in locationsCount) {
      if (locationsCount.hasOwnProperty(location)) {
        total += locationsCount[location];
      }
    }
    return {locationsCount, max, total};
  }

  gradient(n: number) {
    return interpolate('gold', 'green')(n);
  }

  async drawChart() {
    if (this.trips === undefined) {
      await this.refreshTrips();
    }

    this.path = geoPath()
      .projection(geoIdentity()
        .reflectY(true)
        .fitSize([this.width, this.height], topojson
          .feature(this.features, this.features.objects.features)));

    this.zoom = zoom()
      .scaleExtent([1, 8])
      .on('zoom', () => this.zoomListener());
    this.chart.on('click', () => this.reset());

    const locations = this.frequencyLocation(this.trips);
    this.imageCount = locations.total;

    this.chart.select('#background')
      .attr('width', this.width)
      .attr('height', this.height)
      .style('fill', 'none')
      .style('pointer-events', 'all');

    // Fill in all the countries with shading and borders
    this.chart.select('#fill')
      .attr('cursor', 'pointer')
      .selectAll('path')
      .data(topojson.feature(this.features, this.features.objects.features).features)
      .join('path')
      .attr('fill', (d: any) => locations.locationsCount[d.properties.name] ?
        this.gradient(locations.locationsCount[d.properties.name] / locations.max) : 'white')
      .on('click', d => this.clicked(d))
      .attr('d', this.path)
      .attr('stroke', (d: any) => d.properties.admin ? 'silver' : 'black')
      .on('mouseenter', (d: any) => {
        this.title = d.properties.name;
        this.admin = d.properties.admin;
        this.imageCount = locations.locationsCount[d.properties.name] ?
          locations.locationsCount[d.properties.name] : 0;
      })
      .on('mouseout', () => {
        this.title = 'World';
        this.admin = undefined;
        this.imageCount = locations.total;
      });

    // Add a solid border to the external borders of subdivided countries
    this.chart.select('#country')
      .attr('fill', 'none')
      .attr('stroke-linejoin', 'round')
      .attr('d', this.path(topojson.mesh(this.features, this.features.objects.features, (a, b) => {
        if (a === b && a.properties.admin) {
          return true;
        }
        if (a.properties.admin || b.properties.admin) {
          return a.properties.admin !== b.properties.admin;
        }
        return false;
      })))
      .attr('stroke', 'black');

    // Add the small countries as points
    this.chart.select('#point')
      .attr('cursor', 'pointer')
      .selectAll('path')
      .data(topojson.feature(this.points, this.points.objects.points).features)
      .join('path')
      .attr('stroke', 'black')
      .attr('fill', (d: any) => locations.locationsCount[d.properties.name] ?
        this.gradient(locations.locationsCount[d.properties.name] / locations.max) : 'white')
      .on('click', d => this.clicked(d))
      .attr('d', this.path.pointRadius(2.5))
      .on('mouseenter', (d: any) => {
        this.title = d.properties.name;
        this.admin = d.properties.admin;
        this.imageCount = locations.locationsCount[d.properties.name] ?
          locations.locationsCount[d.properties.name] : 0;
      })
      .on('mouseout', () => {
        this.title = 'World';
        this.admin = undefined;
        this.imageCount = locations.total;
      });

    this.chart.call(this.zoom);
  }

  reset() {
    this.selectedCountry = 'World';
    this.admin = undefined;
    this.chart.transition().duration(1000).call(
      this.zoom.transform,
      zoomIdentity,
      zoomTransform(this.chart.node()).invert([this.width / 2, this.height / 2])
    );
  }

  clicked(d: any) {
    this.selectedCountry = d.properties.name;
    this.admin = d.properties.admin;

    const [[x0, y0], [x1, y1]] = this.path.bounds(d);
    event.stopPropagation();

    this.chart.transition().duration(1000).call(
      this.zoom.transform,
      zoomIdentity
        .translate(this.width / 2, this.height / 2)
        .scale(Math.min(8, 0.9 / Math.max((x1 - x0) / this.width, (y1 - y0) / this.height)))
        .translate(-(x0 + x1) / 2, -(y0 + y1) / 2),
      mouse(this.chart.node())
    );
  }

  zoomListener() {
    const transform = event.transform;
    this.chart.attr('transform', transform);
    this.chart.attr('stroke-width', 1 / transform.k);
  }
}
