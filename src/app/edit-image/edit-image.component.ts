import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {ImageInterface} from '../_models/trip.interface';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../_services/api.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {locations, filter, locationValidator} from '../_models/locations.list';

@Component({
  selector: 'app-edit-image',
  templateUrl: './edit-image.component.html',
  styleUrls: ['./edit-image.component.css']
})
export class EditImageComponent implements OnInit {
  image: ImageInterface;
  editImageForm: FormGroup;
  locations: string[];
  locationsObservable: Observable<string[]>;
  loading: boolean;

  constructor(private dialogRef: MatDialogRef<EditImageComponent>,
              @Inject(MAT_DIALOG_DATA) data,
              private formBuilder: FormBuilder,
              private apiService: ApiService) {
    this.image = data.image;

    this.locations = locations;

    this.editImageForm = this.formBuilder.group({
      date: [new Date(this.image.dateString), Validators.required],
      location: new FormControl(this.image.location, [
        locationValidator()
      ]),
    });

    this.locationsObservable = this.editImageForm.get('location').valueChanges
        .pipe(
            startWith(''),
            map(value => filter(value))
        );
  }

  ngOnInit(): void {
  }

  saveImage(){
    if (this.editImageForm.invalid) {
      return;
    }
    this.loading = true;
    if (new Date(this.image.dateString).toISOString() === this.editImageForm.value.date.toISOString() &&
        this.image.location === this.editImageForm.value.location) {
      this.dialogRef.close(false);
    }
    else {
      this.apiService.editImage(
        this.image.name,
        this.editImageForm.value.date,
        this.editImageForm.value.location)
        .subscribe(() => {
          this.image.date = this.editImageForm.value.date;
          this.image.dateString = this.image.date.toISOString();
          this.image.location = this.editImageForm.value.location;
          this.dialogRef.close(true);
        });
    }
  }
}
