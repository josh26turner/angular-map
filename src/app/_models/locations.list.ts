import * as features from '../../assets/features-simplified.json';
import * as points from '../../assets/points-topo.json';
import {AbstractControl, ValidatorFn} from '@angular/forms';

function case_insensitive_comp(strA, strB) {
  return strA.toLowerCase().localeCompare(strB.toLowerCase());
}

export let locations = [];
// @ts-ignore
for (const geometry of features.default.objects.features.geometries) {
  locations.push(geometry.properties.name);
}
// @ts-ignore
for (const geometry of points.default.objects.points.geometries) {
  locations.push(geometry.properties.name);
}
locations.sort(case_insensitive_comp);
locations = [... new Set(locations)];

export function filter(value: string): string[] {
  return locations.filter(option => option.normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .toLowerCase()
    .includes(value.toLowerCase()));
}

export function locationValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const forbidden = !locations.includes(control.value);
    return forbidden ? {forbiddenLocation: {value: control.value}} : null;
  };
}
