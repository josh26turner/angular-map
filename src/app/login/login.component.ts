import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AuthenticationService} from '../_services/authentication.service';
import {AlertService} from '../_services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private authService: AuthenticationService,
              private alertService: AlertService) {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    if (this.authService.currentUserValue && this.authService.currentUserValue.token) {
      window.location.href = '/map';
    }
  }

  login(): void {
    if (this.loginForm.value.username && this.loginForm.value.password) {
      this.authService.login(this.loginForm.value.username,
        this.loginForm.value.password)
        .subscribe(
          () => {
            window.location.href = '/map';
          },
          (error) => {
            this.alertService.error(error.error);
          }
        );
    }
  }

}
