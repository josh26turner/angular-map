![](https://gitlab.com/josh26turner/angular-map/-/raw/master/src/assets/example.png "Example Map")

# Angular and D3 Mapping tool

Example `docker-compose.yml`:
```
version: "3.7"
services:
    papa:
        image: josh26turner/papa
        container_name: papa
        environment:
          - JWT_SECRET=<your_random_jwt_secret>
        volumes:
          - /path/to/config:/config
          - /path/to/images:/images
        restart: unless-stopped
```
