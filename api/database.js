const sqlite3 = require('sqlite3').verbose();

const configPath = require('./config').configPath

const userdb = new sqlite3.Database(configPath + 'userdb.sqlite');
const tripsdb = new sqlite3.Database(configPath + 'trips.sqlite');

userdb.run(`CREATE TABLE user (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        username text NOT NULL UNIQUE,
        password text NOT NULL
        )`,
  (err) => {
    if (err) {
      // Table already created
    } else {
      // Table just created
    }
  }
);

tripsdb.run(`CREATE TABLE images (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user INTEGER NOT NULL,
        name TEXT NOT NULL UNIQUE,
        date TEXT NOT NULL,
        location TEXT NOT NULL,
        trip INTEGER NOT NULL
        )`,
  (err) => {
    if (err) {
      // Table already created
    } else {
      // Table just created
    }
  }
);

tripsdb.run(`CREATE TABLE trips (
        id INTEGER PRIMARY KEY AUTOINCREMENT,
        user INTEGER NOT NULL,
        name TEXT NOT NULL,
        description TEXT DEFAULT NULL
        )`,
  (err) => {
    if (err) {
      // Table already created
    } else {
      // Table just created
    }
  })

tripsdb.run(`CREATE TABLE tripsUser (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            user INTEGER NOT NULL,
            trip INTEGER NOT NULL
            )`,
  () => {})

tripsdb.run(`CREATE TABLE tripInvites (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            user INTEGER NOT NULL,
            trip INTEGER NOT NULL,
            owner INTEGER NOT NULL,
            FOREIGN KEY (trip) REFERENCES trips (id)
            )`,
  () => {})

module.exports = {user: userdb, trips: tripsdb};
