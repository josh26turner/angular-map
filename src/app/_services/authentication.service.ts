import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {apiUrl} from '../config';
import {map} from 'rxjs/operators';
import {UserInterface} from '../_models/user.interface';
import {BehaviorSubject} from 'rxjs';

const userString = 'currentUser';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private currentUser: BehaviorSubject<UserInterface>;

  constructor(private http: HttpClient) {
    this.currentUser = new BehaviorSubject<UserInterface>(JSON.parse(localStorage.getItem(userString)));
  }

  login(username: string, password: string) {
    return this.http.post<UserInterface>(`${apiUrl}/login`, {username, password}).pipe(map(
      (user: UserInterface) => {
        localStorage.setItem(userString, JSON.stringify(user));
        this.currentUser.next(user);
        return user;
      }
    ));
  }

  register(username: string, password: string) {
    return this.http.post<UserInterface>(`${apiUrl}/register`, {username, password}).pipe(map(
      (user: UserInterface) => {
        localStorage.setItem(userString, JSON.stringify(user));
        this.currentUser.next(user);
      }
    ));
  }

  update(username: string, password: string) {
    return this.http.patch<UserInterface>(`${apiUrl}/user`, {username, password}).pipe(map(
      (user: UserInterface) => {
        localStorage.setItem(userString, JSON.stringify(user));
        this.currentUser.next(user);
      }
    ));
  }

  delete() {
    return this.http.delete(`${apiUrl}/user`);
  }

  logout() {
    localStorage.removeItem(userString);
    this.currentUser.next(null);
  }

  get currentUserValue(): UserInterface {
    return this.currentUser.value;
  }
}
