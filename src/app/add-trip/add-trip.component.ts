import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../_services/api.service';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-add-trip',
  templateUrl: './add-trip.component.html',
  styleUrls: ['./add-trip.component.css']
})
export class AddTripComponent implements OnInit {
  addTripForm: FormGroup;
  loading: boolean;

  constructor(private formBuilder: FormBuilder,
              private apiService: ApiService,
              private dialogRef: MatDialogRef<AddTripComponent>) {
    this.addTripForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['']
    });
  }

  ngOnInit(): void {
  }

  addTrip() {
    if (this.addTripForm.invalid) {
      return;
    }
    this.loading = true;
    this.apiService.addTrip(
      this.addTripForm.value.name,
      this.addTripForm.value.description).subscribe((trip) => {
        this.dialogRef.close(trip);
    });
  }
}
