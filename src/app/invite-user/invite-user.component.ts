import { Component, Inject, OnInit } from '@angular/core';
import { ApiService } from '../_services/api.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { TripInterface } from '../_models/trip.interface';
import { UserInterface } from '../_models/user.interface';
import { FormGroup, FormBuilder, FormControl, ValidatorFn, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-invite-user',
  templateUrl: './invite-user.component.html',
  styleUrls: ['./invite-user.component.css']
})
export class InviteUserComponent implements OnInit {
  inviteUserForm: FormGroup;
  users: UserInterface[];
  trip: TripInterface;
  loading: boolean;

  usersObservable: Observable<string[]>;

  constructor(private apiService: ApiService,
              private dialogRef: MatDialogRef<InviteUserComponent>,
              private formBuilder: FormBuilder,
              @Inject (MAT_DIALOG_DATA) data) {
    this.trip = data.trip;
    this.users = data.users;
    this.inviteUserForm = this.formBuilder.group({
      name: new FormControl('', [this.userValidator()])
    });
    this.usersObservable = this.inviteUserForm.get('name').valueChanges
      .pipe(
        startWith(''),
        map(value => this.filter(value))
      );
  }

  userValidator(): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
      const forbidden = !this.users.map(user => user.username).includes(control.value);
      return forbidden ? {notAUser: {value: control.value}} : null;
    };
  }

  async ngOnInit() {
  }

  filter(value: string): string[] {
    return this.users.map(val => val.username).filter(option => option.normalize('NFD')
    .replace(/[\u0300-\u036f]/g, '')
    .toLowerCase()
    .includes(value.toLowerCase()));
  }

  async inviteUserToTrip() {
    if (this.inviteUserForm.invalid) {
      console.error(this.inviteUserForm.invalid);
      return;
    }
    this.loading = true;
    const id = this.users.filter(user => user.username === this.inviteUserForm.value.name)[0].id;
    await this.apiService.inviteUserToTrip(this.trip.id, id).toPromise().then(() => {
      this.dialogRef.close();
    });
    this.loading = false;
  }
}
