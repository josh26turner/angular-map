const express = require('express');
const expressJwt = require('express-jwt');
const fileUpload = require('express-fileupload');

const user = require('./user.api');
const trips = require('./trips.api');
const images = require('./images.api')

const router = express.Router();

const jwtSecret = process.env.JWT_SECRET;

router.use(expressJwt({secret: jwtSecret}).unless({path: ['/api/login', '/api/register']}));
router.use(fileUpload());

router.get('/users', (req, res) => {
  user.getUsers(req, res);
})

router.get('/user', (req, res) => {
  user.getUser(req, res);
})

router.post('/login', (req, res) => {
  user.login(req, res);
});

router.post('/register', (req, res) => {
  user.register(req, res);
});

router.patch('/user', (req, res) => {
  user.update(req, res);
});

router.delete('/user', (req, res) => {
  user.remove(req, res);
});

router.get('/trips', (req, res) => {
  trips.getAll(req, res);
});

router.get('/trip', (req, res) => {
  trips.getTrip(req, res);
});

router.get('/tripUsers', (req, res) => {
  trips.getUsers(req, res);
});

router.post('/trip', (req, res) => {
  trips.addTrip(req, res);
});

router.patch('/trip', (req, res) => {
  trips.updateTrip(req, res);
})

router.delete('/trip', (req, res) => {
  trips.deleteTrip(req, res);
});

router.get('/images', (req, res) => {
  images.getAll(req, res);
});

router.post('/images', (req, res) => {
  images.addImages(req, res);
});

router.patch('/image', (req, res) => {
  images.updateImage(req, res);
});

router.delete('/image', async (req, res) => {
  await images.removeImage(req, res);
});

router.post('/invite', (req, res) => {
  trips.inviteUser(req, res);
})

router.get('/invites', (req, res) => {
  trips.getInvites(req, res);
})

router.post('/acceptInvite', (req, res) => {
  trips.acceptInvite(req, res);
})

router.post('/rejectInvite', (req, res) => {
  trips.rejectInvite(req, res);
})

router.delete('/leaveTrip', (req, res) => {
  trips.leaveTrip(req, res);
})

module.exports = router;
